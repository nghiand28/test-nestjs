import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException } from '@nestjs/common';
import { UserProfilesService } from './user-profiles.service';
import { CreateOwnUserProfileDto, CreateUserProfileDto } from './dto/create-user-profile.dto';
import { UpdateMyUserProfileDto, UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { ApiTags } from '@nestjs/swagger';
import { UseRoles } from 'nest-access-control';
import { Actions, Resources, RoleUUID } from 'src/shared/constant';
import { CurrentUser } from 'src/auth/decorators/current-user.decorator';
import { User, UserType } from 'src/users/user.entity';


@ApiTags('UserProfiles')
@Controller('user-profiles')
export class UserProfilesController {
  constructor(private readonly userProfilesService: UserProfilesService) { }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.CREATE,
    possession: 'any',
  })
  @Post()
  create(@Body() createUserProfileDto: CreateUserProfileDto, @CurrentUser() user: User) {
    return this.userProfilesService.createWithOwner(createUserProfileDto, user);
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.CREATE,
    possession: 'own',
  })
  @Post('my')
  createMyProfile(@Body() createUserProfileDto: CreateOwnUserProfileDto, @CurrentUser() user: User) {
    return this.userProfilesService.createWithOwner({
      userId: user.id,
      ...createUserProfileDto
    }, user);
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.READ,
    possession: 'any',
  })
  @Get()
  findAll() {
    return this.userProfilesService.findAll();
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.READ,
    possession: 'own',
  })
  @Get('my')
  findMyProfile(@CurrentUser() user: User) {
    return this.userProfilesService.findByUserId(user.id);
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.READ,
    possession: 'any',
  })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userProfilesService.findById(id);
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.UPDATE,
    possession: 'own',
  })
  @Patch('my')
  updateMyProfile(@Body() updateUserProfileDto: UpdateMyUserProfileDto, @CurrentUser() user: User) {
    return this.userProfilesService.updateByUserId(user.id, updateUserProfileDto);
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.UPDATE,
    possession: 'any',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserProfileDto: UpdateUserProfileDto) {
    return this.userProfilesService.update(id, updateUserProfileDto);
  }

  @UseRoles({
    resource: Resources.USER_PROFILES,
    action: Actions.DELETE,
    possession: 'any',
  })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userProfilesService.delete(id);
  }
}
