import { HttpException, Injectable, NotFoundException } from '@nestjs/common';
import { CrudService } from 'src/core/base.service';
import { RoleUUID } from 'src/shared/constant';
import { User, UserType } from 'src/users/user.entity';
import { CreateUserProfileDto } from './dto/create-user-profile.dto';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { UserProfile } from './entities/user-profile.entity';

@Injectable()
export class UserProfilesService extends CrudService(UserProfile) {

  // because of this: https://github.com/typeorm/typeorm/issues/2200
  // we need to use find instead
  async findByUserId(id: string) {
    const profiles = await this.repo.find({
      where: {
        userId: id
      }
    })

    return profiles ? profiles[0] : null
  }

  async createWithOwner(createUserProfileDto: CreateUserProfileDto, user: User) {
    const exist = await this.repo.findOne({
      select: ['id'],
      where: {
        userId: user.id
      }
    })
    if (exist) {
      throw new HttpException('User has only one profile', 400)
    }
    let dto: any = {
      ...createUserProfileDto
    }
    if (user.type === UserType.USER) {
      dto = {
        ...dto,
        roles: [{
          id: RoleUUID.USER
        }]
      };
    } else {
      throw new HttpException('Only user can create profile', 400)
    }
    return this.repo.save(dto);
  }

  async update(id: string, updateUserProfileDto: UpdateUserProfileDto) {
    const userProfile = await this.findOne(id);
    if (userProfile) {
      return this.repo.save({ ...userProfile, ...updateUserProfileDto });
    } else {
      throw new NotFoundException(`User profile ${id} not found`)
    }
  }

  async updateByUserId(userId: string, updateUserProfileDto: UpdateUserProfileDto) {
    const userProfile = await this.findByUserId(userId);
    if (userProfile) {
      return this.repo.save({ ...userProfile, ...updateUserProfileDto });
    } else {
      throw new NotFoundException(`User profile for user ${userId} not found`)
    }
  }

}
