import { PartialType } from '@nestjs/mapped-types';
import { OmitType } from '@nestjs/swagger';
import { CreateUserProfileDto } from './create-user-profile.dto';

export class UpdateUserProfileDto extends PartialType(CreateUserProfileDto) { }

export class UpdateMyUserProfileDto extends OmitType(UpdateUserProfileDto, ['userId']) {

}