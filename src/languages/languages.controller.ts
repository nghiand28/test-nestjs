import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UseRoles } from 'nest-access-control';
import { Public } from 'src/auth/decorators/public.decorator';
import { Actions, Resources } from 'src/shared/constant';
import { CreateLanguageDto } from './dto/create-language.dto';
import { UpdateLanguageDto } from './dto/update-language.dto';
import { LanguagesService } from './languages.service';

@ApiTags('Languages')
@Public()
@Controller('languages')
export class LanguagesController {
  constructor(private readonly languagesService: LanguagesService) { }

  @UseRoles({
    resource: Resources.LANGUAGES,
    action: Actions.CREATE,
    possession: 'any',
  })
  @Post()
  create(@Body() createLanguageDto: CreateLanguageDto) {
    return this.languagesService.create(createLanguageDto);
  }

  @UseRoles({
    resource: Resources.LANGUAGES,
    action: Actions.READ,
    possession: 'any',
  })
  @Get()
  findAll() {
    return this.languagesService.findAll();
  }

  @UseRoles({
    resource: Resources.LANGUAGES,
    action: Actions.READ,
    possession: 'any',
  })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.languagesService.findById(id);
  }

  @UseRoles({
    resource: Resources.LANGUAGES,
    action: Actions.UPDATE,
    possession: 'any',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLanguageDto: UpdateLanguageDto) {
    return this.languagesService.update(id, updateLanguageDto);
  }

  @UseRoles({
    resource: Resources.LANGUAGES,
    action: Actions.DELETE,
    possession: 'any',
  })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.languagesService.delete(id);
  }
}
