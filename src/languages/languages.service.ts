import { Injectable } from '@nestjs/common';
import { CrudService } from 'src/core/base.service';
import { Language } from './entities/language.entity';

@Injectable()
export class LanguagesService extends CrudService(Language) { }
