import { Injectable } from '@nestjs/common';
import { CrudService } from 'src/core/base.service';
import { Industry } from './entities/industry.entity';

@Injectable()
export class IndustriesService extends CrudService(Industry) { }
