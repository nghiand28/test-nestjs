import { Type } from '@nestjs/common';
import { EntityNotFoundError, FindConditions, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FindQuery } from './paging';

export interface ICrudService<T> {
  repo: Repository<T>;
  find(query: FindQuery): Promise<T[]>;
  findAll(): Promise<T[]>;
  findById(id: string): Promise<T>;
  findOne(options: unknown): Promise<T>;
  create(dto: Partial<T>): Promise<T>;
  update(id: string, dto: Partial<T>): Promise<T>;
  delete(id: string): Promise<boolean>;
}

export function CrudService(Model: any): Type<ICrudService<typeof Model>> {
  class CrudServiceHost {
    @InjectRepository(Model) repo: Repository<typeof Model>

    async find(query: FindQuery): Promise<typeof Model[]> {
      return this.repo.find({
        where: query.where,
        skip: query.limit * query.page,
        take: query.limit,
        order: { [query.sortBy]: query.sortOrder }
      });
    }

    async findAll(): Promise<typeof Model[]> {
      return this.repo.find();
    }

    async findById(id: string): Promise<typeof Model> {
      return this.repo.findOneOrFail(id);
    }

    async findOne(options: FindConditions<typeof Model>): Promise<typeof Model> {
      return this.repo.findOneOrFail({
        where: options
      });
    }

    async create(dto: any): Promise<typeof Model> {
      return this.repo.save(this.repo.create(dto));
    }

    async update(id: string, dto: Partial<typeof Model>): Promise<any> {
      const result = await this.repo.update(id, dto);
      if (result.affected !== 1) {
        throw new EntityNotFoundError(Model, id);
      }
    }

    async delete(id: string): Promise<any> {
      const result = await this.repo.delete(id);
      if (!result.affected) {
        throw new EntityNotFoundError(Model, id);
      }
    }

  }

  return CrudServiceHost;
}
