export enum Resources {
  AUTH = "AUTH",
  USER = "USER",
  COUNTRIES = "COUNTRIES",
  LANGUAGES = "LANGUAGES",
  INDUSTRIES = "INDUSTRIES",
  USER_PROFILES = "USER_PROFILES",
  COMPANY_PROFILES = "COMPANY_PROFILES",
}

export enum Roles {
  USER = 'USER',
  HIRER = 'HIRER',
  ADMIN = 'ADMIN'
}

export enum Actions {
  READ = "read",
  CREATE = "create",
  UPDATE = "update",
  DELETE = "delete"
}

export enum RoleUUID {
  USER = '90df268d-0947-11ec-9b25-0242ac140002',
  HIRER = '0378cee7-0948-11ec-9b25-0242ac140002',
  ADMIN = '09fa1762-0948-11ec-9b25-0242ac140002'
}