import { Injectable } from '@nestjs/common';
import { CrudService } from 'src/core/base.service';
import { Country } from './entities/country.entity';

@Injectable()
export class CountriesService extends CrudService(Country) { }
