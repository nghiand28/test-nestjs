import { Injectable } from '@nestjs/common';
import { CrudService } from 'src/core/base.service';
import { CompanyProfile } from './entities/company-profile.entity';

@Injectable()
export class CompanyProfilesService extends CrudService(CompanyProfile){}
